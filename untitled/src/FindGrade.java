public class FindGrade {

    public static void main (String[] args) {
        double x = Integer.parseInt(args[0]);
        if (x <= 100 && x >= 90) {
            System.out.println("Your grade is A");
        } else if (90 > x && x >= 80) {
            System.out.println("Your grade is B");
        } else if (80 > x && x >= 70) {
            System.out.println("Your grade is C");
        } else if (70 > x && x >= 60) {
            System.out.println("Your grade is D");
        } else if (x < 60 && x >= 0) {
            System.out.println("Your grade is F");
        } else {
            System.out.println("It is not a valid score!");
        }
    }

}
